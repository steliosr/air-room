package main

import (
	"fmt"
	"net/http"
	"text/template"
)

var portNumber string = ":8080"

func Home(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "home.page.tmpl")
}

func About(w http.ResponseWriter, r *http.Request) {
	renderTemplate(w, "about.page.tmpl")
}

func renderTemplate(w http.ResponseWriter, tmpl string) {
	parsedTemplate, parseErr := template.ParseFiles("templates/" + tmpl)
	if parseErr != nil {
		fmt.Println("Could not parse file!", tmpl)
		return
	}
	err := parsedTemplate.Execute(w, nil)
	if err != nil {
		fmt.Println("error parsing template", err)
	}
}

func main() {
	http.HandleFunc("/", Home)
	http.HandleFunc("/about", About)
	fmt.Println("Starting application on port" + portNumber)
	_ = http.ListenAndServe(portNumber, nil)
}
